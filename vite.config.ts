import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import * as path from 'path';
import { resolve } from 'path';
const projectRootDir = path.resolve(__dirname);
export default defineConfig({
    plugins: [
        vue({
            template: {}
        })
    ],
    resolve: {
        alias: {
            '@': resolve(projectRootDir, 'src'),
            '@Forms': resolve(projectRootDir, 'src/forms/'),
            '@Fields': resolve(projectRootDir, 'src/fields/'),
            '@Functions': resolve(projectRootDir, 'src/functions/'),
            '@Templates': resolve(projectRootDir, 'src//templates/'),
            '@Components': resolve(projectRootDir, 'src//components/'),
            '@Composables': resolve(projectRootDir, 'src//composables/'),
            '@Public': resolve(projectRootDir, 'src//public/'),
            '@Pages': resolve(projectRootDir, 'src//pages'),
            '@ApiTalentHive': resolve(projectRootDir, 'src/api/talenthive')
        },
        extensions: ['*', '.js', '.vue', '.json', '.ts']
    }
});
