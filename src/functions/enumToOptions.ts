const enumToOptions = (enums: any) => {
    const array = [];
    Object.keys(enums).forEach(function (key, index) {
        array.push({ name: enums[key], value: key });
    });
    return array;
};

export default enumToOptions;
