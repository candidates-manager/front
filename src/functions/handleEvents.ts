import { emitter } from '@/bus';
import { useDebounceFn } from '@vueuse/core';

//Success
export const handleSuccess = useDebounceFn((message: string = ''): void => {
    if (message) {
        emitter.emit('updateStatus', {
            type: 'success',
            message: message
        });
    }
}, 100);

//Error
export const handleError = useDebounceFn(
    (error: Error, message: string = '') => {
        emitter.emit('updateStatus', {
            type: 'error',
            message: error.message || message
        });
        if (import.meta.env.VITE_NODE_ENV === 'development') {
            console.log(error, message);
        }
    },
    100
);
