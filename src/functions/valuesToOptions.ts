export const valuesToOptions = (array) => {
    const arrayOptions = [];
    for (let i = 0; i < array.length; i++) {
        arrayOptions.push({
            name: array[i].toString(),
            value: array[i]
        });
    }
    return arrayOptions;
};
