import { UserStore } from './store';
import { createRouter, createWebHistory } from 'vue-router';

//Templates
import DefaultTemplate from '@Templates/Dashboard.Template.vue';
import AuthTemplate from '@Templates/Auth.Template.vue';
import CandidateTemplate from '@Templates/Candidate.Template.vue';
import OnBoardingTemplate from '@Templates/OnBoarding.Template.vue';

//Dashboard
import Homepage from '@Pages/Homepage.Page.vue';
import JobsPage from '@Pages/Jobs/Jobs.Page.vue';
import JobsSinglePage from '@Pages/Jobs/Jobs.Single.Page.vue';

//Application
import ApplicationsPage from '@Pages/Applications/Applications.List.Page.vue';
import ApplicationSinglePage from '@Pages/Applications/Applications.Single.Page.vue';

//Candidate
import CandidatesPage from '@Pages/Candidates/Candidates.Page.vue';
import CandidatesSinglePage from '@Pages/Candidates/Candidates.Single.Page.vue';
import CandidateApplicationPage from '@Pages/Candidates/Candidates.Application.Page.vue';

//Users
import UsersPage from '@Pages/Users/Users.Page.vue';

//Process
import ProcessesPage from '@Pages/ApplicationProcess/ApplicationProcess.Page.vue';
import ProcessesSinglePage from '@Pages/ApplicationProcess/ApplicationProcess.Single.Page.vue';

//Account
import AccountPage from './pages/Account.Page.vue';
import SettingsPage from '@Pages/Settings.Page.vue';

//Auth
import LoginForm from '@Forms/Auth/Login.Form.vue';
import LostPassword from '@Forms/Auth/LostPassword.Form.vue';

//Others
import FAQPage from '@Pages/FAQ.Page.vue';
import ContactPage from '@Pages/Contact.Page.vue';
import NotFoundTemplate from '@Templates/NotFound.Template.vue';

//Forms
import UserForm from '@Forms/User/User.Edit.Form.vue';
import PasswordForm from '@/forms/User/Password.Form.vue';

// TODO-- CONTROLLER LES AUTORISATIONS PAR RAPPORT AUX ROLES ICI

// Router
const router = createRouter({
    history: createWebHistory('/'),
    routes: [
        {
            path: '/',
            component: DefaultTemplate,
            meta: { requiresAuth: true },
            children: [
                {
                    path: '/',
                    meta: { requiresAuth: true },
                    component: Homepage
                },
                {
                    path: '/account',
                    meta: { requiresAuth: true },
                    component: AccountPage
                },
                {
                    path: '/settings',
                    meta: { requiresAuth: true, isOnboarding: false },
                    component: SettingsPage
                },
                {
                    path: '/jobs',
                    meta: { requiresAuth: true },
                    component: JobsPage
                },
                {
                    path: '/jobs/v/:_id',
                    meta: { requiresAuth: true },
                    component: JobsSinglePage
                },
                {
                    path: '/applications',
                    meta: { requiresAuth: true },
                    component: ApplicationsPage
                },
                {
                    path: '/applications/v/:_id',
                    meta: { requiresAuth: true },
                    component: ApplicationSinglePage
                },
                {
                    path: '/candidates',
                    meta: { requiresAuth: true },
                    component: CandidatesPage
                },
                {
                    path: '/candidates/v/:_id',
                    meta: { requiresAuth: true },
                    component: CandidatesSinglePage
                },
                {
                    path: '/processes',
                    meta: { requiresAuth: true },
                    component: ProcessesPage
                },
                {
                    path: '/processes/v/:_id',
                    meta: { requiresAuth: true },
                    component: ProcessesSinglePage
                },
                {
                    path: '/users',
                    meta: { requiresAuth: true },
                    component: UsersPage
                },
                {
                    path: '/faq',
                    meta: { requiresAuth: true },
                    component: FAQPage
                },
                {
                    path: '/contact-us',
                    meta: { requiresAuth: true },
                    component: ContactPage
                }
            ]
        },
        {
            path: '/auth',
            name: 'Auth',
            meta: { template: 'AuthTemplate', requiresAuth: false },
            component: AuthTemplate,
            children: [
                {
                    path: 'login',
                    component: LoginForm
                },
                {
                    path: 'lost-password',
                    component: LostPassword
                }
            ]
        },
        {
            path: '/onboarding',
            meta: {
                template: 'OnBoarding',
                isOnboarding: true,
                requiresAuth: true
            },
            component: OnBoardingTemplate,
            children: [
                {
                    path: '',
                    component: UserForm,
                    meta: {
                        step: 0
                    }
                },
                {
                    path: 'password',
                    component: PasswordForm,
                    meta: {
                        step: 1
                    }
                }
            ]
        },
        {
            path: '/my-application',
            meta: { template: 'CandidateTemplate', requiresAuth: false },
            component: CandidateTemplate,
            children: [
                {
                    path: ':_id',
                    component: CandidateApplicationPage
                }
            ]
        },
        {
            path: '/:notFound',
            meta: { template: 'NotFound', requiresAuth: true },
            component: NotFoundTemplate
        }
    ]
});

router.beforeEach(async (to, from, next) => {
    const { path } = to;
    const userStore = UserStore();
    const isUserLoggedIn = userStore.isUserLoggedIn;
    const user_status = userStore.getUserStatus;

    //User Not Logged In
    if (!isUserLoggedIn) {
        //Requires An Authentification
        if (to.meta.requiresAuth === true && path !== '/auth/login') {
            next('auth/login');
        }
        //Does not require an Authentification
        else {
            next();
        }
    }

    //User Status is new
    else if (isUserLoggedIn && user_status === 'onboarding') {
        if (to.meta.isOnboarding === true) {
            next();
        } else {
            next('onboarding');
        }
    }

    //User Logged In
    else {
        if (path === '/auth/login' || to.meta.isOnboarding === true) {
            next('/');
        } else {
            next();
        }
    }
});
export default router;
