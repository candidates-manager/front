//API
import { ApiGetRoles } from '@/api/talenthive/Role/Get.Roles.Api';

export const getRoles = async (rolesPublicId: string[]) => {
    const roles = await ApiGetRoles({ public_id: rolesPublicId });
    let options = [];
    if (roles instanceof Error) {
        return options;
    } else {
        options = roles.map((c) => {
            return {
                name: c.name,
                value: c._id
            };
        });
    }
    return options;
};
