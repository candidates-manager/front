import * as yup from 'yup';
const EmployeeSettingsFields = [
    {
        label: 'Linkedin',
        name: 'linkedin',
        as: 'inputText',
        type: 'url',
        selectedValue: 'linkedin.com/in/',
        editable: false,
        placeholder: 'Linkedin of the company',
        rules: yup.string().url().required().label('Linkedin')
    }
];

export default EmployeeSettingsFields;
