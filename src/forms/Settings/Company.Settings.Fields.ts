import * as yup from 'yup';
const CompanySettingsFields = [
    {
        label: 'Name',
        name: 'name',
        as: 'inputText',
        type: 'text',
        selectedValue: '',
        editable: true,
        placeholder: 'Name of your company',
        rules: yup.string().required().label("Company's name")
    },
    {
        label: 'Linkedin',
        name: 'linkedin',
        as: 'inputText',
        type: 'url',
        // selectedValue: 'linkedin.com/in/',
        editable: true,
        placeholder: 'Linkedin of the company',
        rules: yup.string().url().label('Linkedin Url')
    },
    {
        label: 'Glassdoor',
        name: 'glassdoor',
        as: 'inputText',
        type: 'url',
        // selectedValue: 'glassdoor.com/',
        editable: true,
        placeholder: 'Glassdoor of your company',
        rules: yup.string().url().label('Glassdoor Url')
    }
];

export default CompanySettingsFields;
