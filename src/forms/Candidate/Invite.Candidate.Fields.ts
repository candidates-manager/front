import * as yup from 'yup';

//Functions
import enumToOptions from '@/functions/enumToOptions';

//Options
import { ApplicationOrigin } from 'talenthive-models/lib/Enums';
import { getOptionsCompanyJobs } from '@/forms/Job/Jobs.Company.Options';

export const InviteCandidateFields = async () => {
    return [
        {
            label: 'Email of your candidate',
            name: 'email',
            as: 'inputText',
            type: 'text',
            selectedValue: import.meta.env.VITE_DEFAULT_EMAIL,
            editable: false,
            placeholder: 'Your Email address',
            rules: yup.string().required().email().label('Email')
        },
        {
            label: 'Firstname',
            name: 'firstname',
            as: 'inputText',
            type: 'text',
            selectedValue: import.meta.env.VITE_DEFAULT_FIRSTNAME,
            editable: false,
            placeholder: "Candidate's First Name",
            rules: yup.string().min(3).required().label("Candidate's Firstname")
        },
        {
            label: 'Lastname',
            name: 'lastname',
            as: 'inputText',
            type: 'text',
            selectedValue: import.meta.env.VITE_DEFAULT_LASTNAME,
            editable: false,
            placeholder: "Candidate's Last Name",
            rules: yup.string().min(3).required().label("Candidate's Lastname")
        },
        {
            label: 'LinkedIn',
            name: 'linkedin',
            as: 'inputText',
            type: 'url',
            selectedValue: import.meta.env.VITE_DEFAULT_LINKEDIN_PROFILE,
            editable: false,
            placeholder: '',
            rules: yup.string().url().required().label('Linkedin')
        },
        {
            label: 'Job',
            name: 'job_id',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            editable: false,
            options: await getOptionsCompanyJobs(),
            placeholder: "Select the job you're hiring",
            rules: yup.string().required().label('Job')
        },
        {
            label: 'Origin of the application',
            name: 'origin',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            editable: true,
            options: enumToOptions(ApplicationOrigin),
            placeholder: 'How did you find this Candidate ?',
            rules: yup.string().required().label("Application's Origin")
        }
    ];
};
