import * as yup from 'yup';
const PasswordFields = [
    {
        label: 'Password',
        name: 'password',
        ref: 'password',
        as: 'inputText',
        type: 'password',
        editable: true,
        selectedValue: import.meta.env.VITE_DEFAULT_PASSWORD,
        placeholder: 'Your Password',
        rules: yup.string().required().label('Your Password').min(8)
    },
    {
        label: 'Confirm Your Password',
        name: 'confirmPassword',
        as: 'inputText',
        ref: 'confirmPassword',
        type: 'password',
        selectedValue: import.meta.env.VITE_DEFAULT_PASSWORD,
        placeholder: 'Confirm your Password',
        editable: true,
        rules: yup
            .string()
            .required()
            .label('Confirm Your Password')
            .oneOf([yup.ref('password'), null], 'Passwords must match')
    }
];

export { PasswordFields };
