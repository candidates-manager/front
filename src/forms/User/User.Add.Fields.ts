import * as yup from 'yup';

//API
import { ApiGetUsers } from '@ApiTalentHive/User/Get.Users.Api';

//Options
import { getRoles } from '../Roles/Roles.Options';
import { getOptionsUserCompanies } from '../Company/User.Companies.Option';

import { useDebounceFn, useMemoize } from '@vueuse/core';
import { UserResponseModel } from 'talenthive-models/lib/Entities';

const checkEmail = useDebounceFn(async (value) => {
    //For Emails
    const users = <UserResponseModel[]>await ApiGetUsers(`email=${value}`);

    if (users.length > 0) {
        return false;
    } else {
        return true;
    }
}, 500);

const validateEmail: any = useMemoize(
    async (value) =>
        new Promise(async (resolve, reject) => {
            resolve(checkEmail(value));
        })
);

export const UserAddFields = async () => {
    return [
        {
            label: 'Email',
            name: 'email',
            as: 'inputText',
            type: 'text',
            selectedValue: import.meta.env.VITE_DEFAULT_EMAIL,
            editable: false,
            placeholder: 'Your Email address',
            rules: yup
                .string()
                .required()
                .email()
                .label('Your Email address')
                .test(
                    'isEmailUnique',
                    'This email is already taken',
                    validateEmail
                )
        },
        {
            label: 'Firstname',
            name: 'firstname',
            as: 'inputText',
            type: 'text',
            selectedValue: import.meta.env.VITE_DEFAULT_FIRSTNAME,
            editable: true,
            placeholder: 'Your firstname',
            rules: yup.string().required().min(3).label('Your Name')
        },
        {
            label: 'Lastname',
            name: 'lastname',
            as: 'inputText',
            type: 'text',
            selectedValue: import.meta.env.VITE_DEFAULT_LASTNAME,
            editable: true,
            placeholder: 'Your lastname',
            rules: yup.string().required().min(3).label('Your Name')
        },
        {
            label: 'Role',
            name: 'role_id',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            editable: false,
            options: await getRoles(['recruiter', 'employee']),
            placeholder: "User's role",
            rules: yup.string().required().label('Role')
        },
        {
            label: 'Company',
            name: 'company_id',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            editable: false,
            placeholder: 'Company',
            options: await getOptionsUserCompanies(),
            rules: yup.string().required().label('Company')
        }
    ];
};
