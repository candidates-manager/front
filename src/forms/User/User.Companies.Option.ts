//API
import { ApiFetchCompanyUsers } from '@ApiTalentHive/User/Fetch.Company.Users.Api';
export const getOptionsCompanyUsers = async () => {
    const users = await ApiFetchCompanyUsers();
    let options = [];
    if (users instanceof Error) {
        return options;
    } else {
        options = users.map((c) => {
            return {
                name: c.firstname + ' ' + c.lastname,
                value: c._id
            };
        });
    }
    return options;
};
