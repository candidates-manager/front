import * as yup from 'yup';
export const UserEditFields = [
    {
        label: 'Firstname',
        name: 'firstname',
        as: 'inputText',
        type: 'text',
        editable: true,
        placeholder: '',
        rules: yup.string().required().min(3).label('Firstname')
    },
    {
        label: 'Lastname',
        name: 'lastname',
        as: 'inputText',
        type: 'text',
        editable: true,
        placeholder: '',
        rules: yup.string().required().min(3).label('Lastname')
    },
    {
        label: 'Function',
        name: 'function',
        as: 'inputText',
        type: 'text',
        editable: true,
        placeholder: 'Developer, Product owner, UX/UI Designer...',
        rules: yup.string().min(3).label('Function')
    },
    {
        label: 'LinkedIn',
        name: 'linkedin',
        addon: '',
        as: 'inputText',
        type: 'url',
        selectedValue: import.meta.env.VITE_DEFAULT_LINKEDIN_PROFILE,
        editable: false,
        placeholder: '',
        rules: yup.string().url().label('Linkedin')
    }
];
