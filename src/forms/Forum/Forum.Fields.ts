import * as yup from 'yup';
const LoginFields = [
    {
        label: 'Reply',
        name: 'content',
        as: 'inputText',
        type: 'editor',
        selectedValue: '',
        editable: true,
        placeholder: 'Comment',
        rules: yup.string().required().label('Reply')
    }
];

export default LoginFields;
