import * as yup from 'yup';

//Functions
import enumToOptions from '@/functions/enumToOptions';

//Options
import { ApplicationStatusEnum } from 'talenthive-models/lib/Enums';

export const ApplicationStatusFields = {
    label: '',
    name: 'status',
    as: 'inputSelect',
    type: 'dropdown',
    selectedValue: '',
    editable: true,
    placeholder: '',
    options: enumToOptions(ApplicationStatusEnum),
    rules: yup.string().required().label("Recruiter's reasons")
};
