import * as yup from 'yup';
const LoginFields = [
    {
        label: 'Give the reasons of your decision to the candidate',
        name: 'feedback_reasons',
        as: 'inputText',
        type: 'textarea',
        selectedValue: '',
        editable: true,
        placeholder: 'Give your reasons.',
        rules: yup.string().required().label("Recruiter's reasons")
    },
    {
        label: 'What were the strenghts of the candidate ?',
        name: 'feedback_strenghts',
        as: 'inputText',
        type: 'textarea',
        selectedValue: '',
        editable: true,
        placeholder: "Candidate's Strengths",
        rules: yup.string().label("Candidate's Strengths")
    },
    {
        label: 'What could the candidate  improve ?',
        name: 'feedback_improvements',
        as: 'inputText',
        type: 'textarea',
        selectedValue: '',
        editable: true,
        placeholder: "Candidate's Improvements",
        rules: yup.string().label("Candidate's Improvements")
    },
    {
        label: 'Ask the candidate a review on Glassdoor',
        name: 'request_glassdoor',
        as: 'inputText',
        type: 'toggle',
        selectedValue: '',
        editable: true,
        rules: yup.boolean()
    }
];

export default LoginFields;
