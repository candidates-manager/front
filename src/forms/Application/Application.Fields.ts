import * as yup from 'yup';

//Functions
import enumToOptions from '@/functions/enumToOptions';

//Options
import { ApplicationOrigin } from 'talenthive-models/lib/Enums';
import { getOptionsCompanyUsers } from '@/forms/User/User.Companies.Option';

export const ApplicationFields = async () => {
    return [
        {
            label: 'Start',
            name: 'startAt',
            as: 'inputText',
            type: 'date',
            selectedValue: '',
            editable: true,
            placeholder: 'Starting At',
            rules: yup.date().required().label('Start at')
        },
        {
            label: 'Origin of the application',
            name: 'origin',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            editable: true,
            options: enumToOptions(ApplicationOrigin),
            placeholder: 'How did you find this Candidate ?',
            rules: yup.string().required().label("Application's Origin")
        }
    ];
};
