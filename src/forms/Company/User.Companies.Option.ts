//API
import { ApiFetchUserCompanies } from '@/api/talenthive/Company/Fetch.User.Companies.Api';

export const getOptionsUserCompanies = async () => {
    const companies = await ApiFetchUserCompanies();
    let options = [];
    if (companies instanceof Error) {
        return options;
    } else {
        options = companies.map((c) => {
            return {
                name: c.name,
                value: c._id
            };
        });
    }
    return options;
};
