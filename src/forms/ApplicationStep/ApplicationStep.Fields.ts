import * as yup from 'yup';

//Functions
import { valuesToOptions } from '@/functions/valuesToOptions';
//Options
import { getOptionsCompanyUsers } from '@/forms/User/User.Companies.Option';

export const ApplicationStepFields = async (params: {
    availableSteps: number[];
}) => {
    return [
        {
            label: 'Title',
            name: 'name',
            as: 'inputText',
            type: 'text',
            selectedValue: '',
            editable: true,
            placeholder: 'Title',
            rules: yup.string().required().label('Step')
        },
        {
            label: 'Step',
            name: 'step',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            options: valuesToOptions(params.availableSteps),
            filter: false,
            editable: true,
            placeholder: 'Step',
            rules: yup.number().required().label('Step')
        },
        {
            label: 'Collaborators involved',
            name: 'users',
            as: 'inputSelect',
            type: 'multiselect',
            selectedValue: '',
            editable: true,
            options: await getOptionsCompanyUsers(),
            placeholder: 'Select an User',
            rules: yup.array().label('User')
        },
        {
            label: 'Duration',
            name: 'duration',
            as: 'inputText',
            type: 'number',
            addon: 'days',
            editable: true,
            placeholder: '',
            selectedValue: 5,
            rules: yup.number().required().label('Duration')
        },
        {
            label: 'Description',
            name: 'description',
            as: 'inputText',
            type: 'textarea',
            addon: '',
            selectedValue: '',
            editable: true,
            placeholder: '',
            rules: yup.string().label('Description')
        }
    ];
};
