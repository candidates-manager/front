import * as yup from 'yup';
const LoginFields = [
    {
        label: 'Email',
        name: 'email',
        as: 'inputText',
        type: 'text',
        selectedValue: import.meta.env.VITE_DEFAULT_EMAIL,
        editable: false,
        placeholder: 'Your Email address',
        rules: yup.string().required().email().label('Your Email address')
    },
    {
        label: 'Password',
        name: 'password',
        as: 'inputText',
        type: 'password',
        editable: false,
        selectedValue: import.meta.env.VITE_DEFAULT_PASSWORD,
        placeholder: 'Your Password',
        rules: yup.string().required().label('Your Password')
    }
];

export default LoginFields;
