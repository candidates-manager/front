import * as yup from 'yup';
export const LostPasswordStep2Fields = [
    {
        label: 'Password',
        name: 'password',
        ref: 'password',
        as: 'inputText',
        type: 'password',
        editable: false,
        selectedValue: import.meta.env.VITE_DEFAULT_PASSWORD,
        placeholder: 'Your Password',
        rules: yup.string().required().label('Your Password').min(8)
    },
    {
        label: 'Confirm Your Password',
        name: 'confirmPassword',
        as: 'inputText',
        ref: 'confirmPassword',
        type: 'password',
        selectedValue: import.meta.env.VITE_DEFAULT_PASSWORD,
        placeholder: 'Confirm your Password',
        editable: false,
        rules: yup
            .string()
            .required()
            .label('Confirm Your Password')
            .oneOf([yup.ref('password'), null], 'Passwords must match')
    }
];
