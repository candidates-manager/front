import * as yup from 'yup';
export const LostPasswordStep1Fields = [
    {
        label: 'Enter your Email address',
        name: 'email',
        as: 'inputText',
        type: 'text',
        selectedValue: import.meta.env.VITE_DEFAULT_EMAIL,
        editable: false,
        placeholder: 'Enter your Email address',
        rules: yup.string().required().email().label('Enter your Email address')
    }
];
