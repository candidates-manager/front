import * as yup from 'yup';

export const ApplicationReviewFields = async () => {
    return [
        {
            label: 'Rating',
            name: 'rating',
            as: 'inputText',
            type: 'rating',
            selectedValue: '',
            editable: true,
            placeholder: 'Review',
            rules: yup.number().required().label('Review')
        },
        {
            label: 'Description',
            name: 'description',
            as: 'inputText',
            type: 'editor',
            selectedValue: '',
            editable: true,
            placeholder: 'Review',
            rules: yup.string().required().label('Description')
        }
    ];
};
