import * as yup from 'yup';
//API
import { getOptionsUserCompanies } from '@/forms/Company/User.Companies.Option';

export const ApplicationProcessFields = async () => {
    return [
        {
            label: 'Title',
            name: 'name',
            as: 'inputText',
            type: 'text',
            selectedValue: '',
            placeholder: 'Application Process Title',
            editable: true,
            rules: yup.string().min(6).required().label('Title')
        },
        {
            label: 'Company',
            name: 'company_id',
            as: 'inputSelect',
            type: 'multiselect',
            selectedValue: '',
            editable: true,
            options: await getOptionsUserCompanies(),
            placeholder: 'Select a Company',
            rules: yup.array().min(1).required().label('Company')
        },
        {
            label: 'Steps',
            name: 'steps',
            as: 'inputSteps',
            type: 'steps',
            selectedValue: '',
            editable: true,
            placeholder: 'Steps',
            rules: yup
                .array()
                .min(1, 'At least one step is required!')
                .required()
                .of(
                    yup.object().shape({
                        step: yup.number().required().label('Step number'),
                        duration: yup
                            .number()
                            .default(5)
                            .positive()
                            .integer()
                            .required()
                            .label('Duration'),
                        user: yup.array().label('User'),
                        name: yup.string().required().label('Step title'),
                        description: yup.string()
                    })
                )
                .label('Steps')
        }
    ];
};
