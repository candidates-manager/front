import * as yup from 'yup';
export const ApplicationProcessFields = [
    {
        label: 'Title',
        name: 'name',
        as: 'inputText',
        type: 'text',
        selectedValue: '',
        placeholder: 'Application Process Title',
        editable: true,
        rules: yup.string().min(6).required().label('Job Title')
    },
    {
        label: 'Company',
        name: 'company_id',
        as: 'inputSelect',
        type: 'multiselect',
        selectedValue: '',
        editable: false,
        placeholder: 'Select a Company',
        rules: yup.string().required().label('Company')
    },
    {
        label: 'Steps',
        name: 'steps',
        as: 'inputSteps',
        type: 'steps',
        selectedValue: '',
        editable: false,
        placeholder: 'Steps',
        rules: yup
            .array()
            .min(1, 'At least one step is required!')
            .required()
            .of(
                yup.object().shape({
                    step: yup.number().required().label('Step number'),
                    duration: yup
                        .number()
                        .default(5)
                        .positive()
                        .integer()
                        .required()
                        .label('Duration'),
                    name: yup.string().required().label('Step title'),
                    description: yup.string()
                })
            )
            .label('Steps')
    }
];
