//API
import { ApiFetchCompanyProcesses } from '@/api/talenthive/ApplicationProcess/Fetch.Company.ApplicationProcesses.Api';

export const getOptionsCompanyProcesses = async () => {
    const processes = await ApiFetchCompanyProcesses();
    let options = [];
    if (processes instanceof Error) {
        return options;
    } else {
        options = processes.map((c) => {
            return {
                name: c.name,
                value: c._id
            };
        });
    }
    return options;
};
