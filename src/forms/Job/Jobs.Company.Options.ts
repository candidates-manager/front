//API
import { ApiFetchCompanyJobs } from '@ApiTalentHive/Job/Fetch.Company.Jobs.Api';

export const getOptionsCompanyJobs = async () => {
    const jobs = await ApiFetchCompanyJobs({
        status: ['active']
    });
    let options = [];
    if (jobs instanceof Error) {
        return options;
    } else {
        options = jobs.map((j) => {
            return {
                name: j.name,
                value: j._id
            };
        });
    }
    return options;
};
