import * as yup from 'yup';

//Options
import { getOptionsCompanyProcesses } from '@Forms/ApplicationProcess/ApplicationProcess.Options';
import { getOptionsUserCompanies } from '@/forms/Company/User.Companies.Option';

export const JobFields = async () => {
    return [
        {
            label: 'Title',
            name: 'name',
            as: 'inputText',
            type: 'text',
            selectedValue: '',
            placeholder: 'Job Title',
            editable: true,
            rules: yup.string().min(6).required().label('Job Title')
        },
        {
            label: 'Company',
            name: 'company_id',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            editable: false,
            placeholder: 'Select a Company',
            options: await getOptionsUserCompanies(),
            rules: yup.string().required().label('Company')
        },
        {
            label: 'Application Process',
            name: 'application_process_id',
            as: 'inputSelect',
            type: 'dropdown',
            selectedValue: '',
            editable: true,
            placeholder: 'Select a Process',
            description:
                'This will import the steps of the process inside of your job. You will be able to edit the process of the job application later.',
            options: await getOptionsCompanyProcesses(),
            rules: yup.string().required().label('Company')
        },
        {
            label: 'Link',
            name: 'link',
            as: 'inputText',
            type: 'url',
            selectedValue: '',
            editable: true,
            placeholder: '',
            description: 'Link of the job description',
            rules: yup.string().url().required()
        }
    ];
};
