import { ref, onBeforeMount } from 'vue';
import router from '@/router';
import { useRouteParams } from '@vueuse/router';

export function getParamId() {
    const _id = ref(null);
    onBeforeMount(async () => {
        const getId = useRouteParams('_id');
        _id.value = <string>getId.value;
        if (!_id.value) router.push('/');
    });
    return _id;
}
