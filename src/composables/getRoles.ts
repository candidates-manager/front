import { ref, onBeforeMount } from 'vue';
import { UserStore, CandidateStore } from '@/store';

export function getRoles() {
    const userStore = UserStore();
    const candidateStore = CandidateStore();
    const isAdmin = ref(false);
    const isRecruiter = ref(false);
    const isCandidate = ref(false);
    const isEmployee = ref(false);
    onBeforeMount(async () => {
        if (userStore.isAdmin) isAdmin.value = true;
        if (userStore.isRecruiter) isRecruiter.value = true;
        if (userStore.isEmployee) isEmployee.value = true;
        if (candidateStore.getCandidateId !== null) isCandidate.value = true;
    });
    return { isAdmin, isRecruiter, isEmployee, isCandidate };
}
