import { defineStore } from 'pinia';

//Entities
import {
    RoleEntity,
    AuthResponseModelCredentials,
    AuthResponseModel,
    UserEntity,
    CompanyEntity,
    AuthInputModel
} from 'talenthive-models/lib/Entities';

//Emitter
import { emitter } from '@/bus';

//Store
import { AppStore } from './app';
import { RecruiterStore } from './recruiter';

//API
import { ApiLoginUser } from '@ApiTalentHive/Auth/Login.Auth.Api';
import ApiCheckToken from '@ApiTalentHive/Auth/CheckToken.Auth.Api';
import { ApiGetCurrentUser } from '@/api/talenthive/User/Get.CurrentUser.Api';
import { handleError, handleSuccess } from '@/functions/handleEvents';

export const UserStore = defineStore('UserStore', {
    state: () => ({
        token: '' as string,
        userLoggedIn: false as boolean,
        credentials: {
            user_id: String as null,
            user_status: String as null,
            companies: [] as CompanyEntity[],
            roles: [] as RoleEntity[]
        }
    }),
    getters: {
        isUserLoggedIn(state) {
            return state.userLoggedIn;
        },
        getUserId(state) {
            return state.credentials.user_id;
        },
        getUserStatus(state) {
            return state.credentials.user_status;
        },
        getToken(state) {
            return state.token;
        },
        getRoles(state) {
            return state.credentials.roles.map((r) => r.public_id);
        },
        getCompanies(state) {
            return state.credentials.companies.map((c) => c._id);
        },
        isAdmin(): (typeof UserStore)['getRoles'] | undefined {
            const roles = this.getRoles as any[];
            return roles.includes('admin') ? true : false;
        },
        isRecruiter(): (typeof UserStore)['getRoles'] | undefined {
            const roles = this.getRoles as any[];
            return roles.includes('recruiter') ? true : false;
        },
        isEmployee(): (typeof UserStore)['getRoles'] | undefined {
            const roles = this.getRoles as any[];
            return roles.includes('employee') ? true : false;
        }
    },
    actions: {
        async loginUser(loginForm: AuthInputModel): Promise<void | Error> {
            try {
                emitter.emit('updateAppLoading', true);
                //Generate Token
                const login = <AuthResponseModel>await ApiLoginUser(loginForm);

                if (!login) throw new Error();

                const { token, credentials } = login;

                //Check Token
                if (!token) {
                    throw new Error();
                }

                //Check User Status
                if (!credentials.status || credentials.status === 'inactive') {
                    throw Error('Your User Status is not active');
                }

                //Update Store
                this.token = token;
                this.userLoggedIn = true;
                localStorage.setItem('token', token);

                //Set Credentials
                this.setCredentials(credentials);

                //Emit Status
                handleSuccess('Welcome to TalentHive !');

                //Redirect
                setTimeout(() => {
                    emitter.emit('updateAppLoading', false);
                    this.router.go(0);
                }, 2000);
            } catch (e) {
                this.userLoggedIn = false;
                handleError(e, e.message || 'Login or password incorrect !');
                emitter.emit('updateAppLoading', false);
            }
        },

        //Logout User
        async logoutUser() {
            const useAppStore = AppStore();

            try {
                await useAppStore.changeAppStatus(true);
                localStorage.removeItem('token');
                this.token = '';
                this.userLoggedIn = false;
                this.router.push({ path: '/auth/login' });
            } catch (e) {
                return e;
            } finally {
                //Change the App Status
                await useAppStore.changeAppStatus(true);
            }
        },

        //Check Token
        async checkToken() {
            const useAppStore = AppStore();
            try {
                await useAppStore.changeAppStatus(false);
                const token = <string>localStorage.getItem('token');

                if (token) {
                    <boolean>await ApiCheckToken();
                    this.userLoggedIn = true;
                    this.token = token;
                    //Get Credentials
                    const credentials = await this.fetchUser();

                    //Set Credentials
                    this.setCredentials(credentials);
                }
            } catch (e) {
                this.logoutUser();
            } finally {
                //Change the App Status
                await useAppStore.changeAppStatus(true);
            }
        },

        //Fetch User
        async fetchUser(): Promise<UserEntity | void> {
            try {
                const User = <UserEntity>await ApiGetCurrentUser(this.token);
                return User;
            } catch (e) {
                this.logoutUser();
            }
        },

        //Set Credentials
        setCredentials(credentials: AuthResponseModelCredentials): void {
            this.credentials.user_id = credentials._id;
            this.credentials.user_status = credentials.status;
            this.credentials.roles = credentials.role_id;
            this.credentials.companies = credentials.company_id;
            //Set Current Company
            if (this.isRecruiter) {
                const recruiterStore = RecruiterStore();
                if (localStorage.getItem('current_company_id')) {
                    recruiterStore.setCurrentCompany(
                        localStorage.getItem('current_company_id')
                    );
                } else if (this.credentials.companies.length > 0) {
                    const current_company_id =
                        this.credentials.companies[0]._id;
                    recruiterStore.setCurrentCompany(current_company_id);
                    localStorage.setItem(
                        'current_company_id',
                        current_company_id
                    );
                }
            }
        },

        //Update User Status
        updateUserStatus(status: string): void {
            this.credentials.user_status = status;
        }
    }
});

export default UserStore;
