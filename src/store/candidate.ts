import { defineStore } from 'pinia';

//Entities
import { CandidateResponseModel } from 'talenthive-models/lib/Entities';

export const CandidateStore = defineStore('CandidateStore', {
    state: () => ({
        candidate_id: null
    }),
    getters: {
        getCandidateId(state): string {
            return <string>state.candidate_id;
        }
    },
    actions: {
        setCandidate(candidate: CandidateResponseModel) {
            this.candidate_id = <string>candidate._id;
        }
    }
});

export default CandidateStore;
