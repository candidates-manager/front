import { defineStore } from 'pinia';

export const RecruiterStore = defineStore('RecruiterStore', {
    state: () => ({
        currentCompany: null
    }),
    getters: {
        getCurrentCompany(state): string {
            return <string>state.currentCompany;
        }
    },
    actions: {
        setCurrentCompany(company_id: string) {
            this.currentCompany = <string>company_id;
        }
    }
});

export default RecruiterStore;
