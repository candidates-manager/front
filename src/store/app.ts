import { defineStore } from 'pinia';

//Store
import UserStore from './user';

export const AppStore = defineStore('AppStore', {
    state: () => ({
        appReady: false as boolean
    }),
    getters: {
        isAppReady(state) {
            return state.appReady;
        }
    },
    actions: {
        async launchApp() {
            try {
                //Check Token
                const userStore = UserStore();
                const checkToken = await userStore.checkToken();

                //Launch The App
                this.appReady = true;
            } catch (error) {
                this.appReady = false;
            }
        },
        async changeAppStatus(value: boolean) {
            this.appReady = value;
        }
    }
});

export default AppStore;
