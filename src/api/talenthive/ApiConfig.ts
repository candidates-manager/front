import { createFetch, useDateFormat } from '@vueuse/core';

export const useMyFetch = createFetch({
    baseUrl: `${import.meta.env.VITE_API_URL}`,
    options: {
        onFetchError(ctx) {
            const data = JSON.parse(ctx.data);
            if (data.code === 'validation/duplicate-fields') {
                //Duplicate Email
                if (data.fields.includes(' email')) {
                    ctx.error = new Error('Email is already used !');
                }
            } else {
                ctx.error = new Error(data.message);
            }
            return ctx;
        },
        async beforeFetch({ options, cancel }) {
            if (!localStorage.getItem('token')) cancel();
            options.headers = {
                ...options.headers,
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            };

            return { options };
        }
    }
});

export const useMyFetchNoToken = createFetch({
    baseUrl: `${import.meta.env.VITE_API_URL}`,
    options: {
        onFetchError(ctx) {
            const data = JSON.parse(ctx.data);
            ctx.error = new Error(data.message);
            return ctx;
        },
        async beforeFetch({ options, cancel }) {
            options.headers = {
                ...options.headers,
                'Content-Type': 'application/json'
            };

            return { options };
        }
    }
});

export const handleResponse = async (response: any): Promise<any | Error> => {
    try {
        //Errors
        if (response.error.value) {
            throw Error(response.error.value.message);
        }

        const jsonData: any = JSON.parse(response.data.value);

        //Check Data
        if (!jsonData.data || jsonData.data === undefined) {
            throw Error('No Data Found');
        }

        //Format Data
        if (jsonData.data.startAt) {
            jsonData.data.startAt = new Date(jsonData.data.startAt);
        }

        return jsonData.data;
    } catch (error) {
        // handleError(error);
        throw Error(error);
    }
};
