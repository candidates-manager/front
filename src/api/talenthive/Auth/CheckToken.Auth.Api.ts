import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

export const ApiCheckToken = async (): Promise<boolean | Error> => {
    try {
        const response: any = await useMyFetch('auth/checkToken', {
            async beforeFetch({ url, options, cancel }) {
                return {
                    options
                };
            }
        }).get();
        const data: boolean = await handleResponse(response);
        return data;
    } catch (e) {
        // handleError(e);
    }
};

export default ApiCheckToken;
