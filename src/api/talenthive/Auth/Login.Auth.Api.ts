//Entities
import {
    AuthResponseModel,
    AuthInputModel
} from 'talenthive-models/lib/Entities';
import { useMyFetchNoToken, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiLoginUser = async (
    credentials: AuthInputModel
): Promise<AuthResponseModel | Error> => {
    try {
        const response: any = await useMyFetchNoToken('auth/login', {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(credentials);
                return {
                    options
                };
            }
        }).post();
        const data: AuthResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
