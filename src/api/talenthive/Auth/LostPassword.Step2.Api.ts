//Entities
import { useMyFetchNoToken, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiLostPasswordStep2 = async (
    token: string,
    password: string
): Promise<boolean | Error> => {
    try {
        const fetch: unknown = await useMyFetchNoToken(
            `auth/change-password?token=${token}`,
            {
                async beforeFetch({ url, options, cancel }) {
                    //Token
                    options.body = JSON.stringify({ password });

                    return {
                        options
                    };
                }
            }
        ).post();
        const data: boolean = await handleResponse(fetch);
        return data;
    } catch (error) {
        handleError(error, error.message);
    }
};
