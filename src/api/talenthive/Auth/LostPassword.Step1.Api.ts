//Entities
import { useMyFetchNoToken, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiLostPasswordStep1 = async (params: {
    email: string;
    url: string;
}): Promise<boolean | Error> => {
    try {
        const fetch: unknown = await useMyFetchNoToken('auth/lost-password', {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).post();
        const data: boolean = await handleResponse(fetch);
        return data;
    } catch (error) {
        handleError(error, 'Error while trying to recover your password !');
    }
};
