//Entities
import {
    ApplicationReviewInputModel,
    ApplicationReviewResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiAddApplicationReview = async (
    application_id: string,
    step_id: string,
    params: ApplicationReviewInputModel
): Promise<ApplicationReviewResponseModel | Error> => {
    try {
        const useUserStore = UserStore();
        const newParams: ApplicationReviewInputModel = {
            ...params,
            step_id,
            user_id: useUserStore.getUserId
        };
        const response: any = await useMyFetch(
            `applications/${application_id}/reviews/add`,
            {
                async beforeFetch({ url, options, cancel }) {
                    options.body = JSON.stringify(newParams);
                    return {
                        options
                    };
                }
            }
        ).post();
        const data: ApplicationReviewResponseModel = await handleResponse(
            response
        );
        return data;
    } catch (error) {
        handleError(error, 'Error while creating your review !');
    }
};
