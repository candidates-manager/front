//Entities
import {
    ApplicationReviewUpdateModel,
    ApplicationReviewResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateApplicationReview = async (
    application_id: string,
    review_id: string,
    params: ApplicationReviewUpdateModel
): Promise<ApplicationReviewResponseModel | Error> => {
    try {
        const response: any = await useMyFetch(
            `applications/${application_id}/reviews/u/${review_id}`,
            {
                async beforeFetch({ url, options, cancel }) {
                    options.body = JSON.stringify(params);
                    return {
                        options
                    };
                }
            }
        ).patch();
        const data: ApplicationReviewResponseModel = await handleResponse(
            response
        );
        return data;
    } catch (error) {
        handleError(error, 'Error while creating your review !');
    }
};
