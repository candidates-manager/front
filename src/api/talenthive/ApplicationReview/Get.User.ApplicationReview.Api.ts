//Entities
import { ApplicationReviewResponseModel } from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

//Store
import { UserStore } from '@/store';

export const ApiGetUserApplicationReview = async (
    application_id: string,
    step_id: string
): Promise<ApplicationReviewResponseModel[] | Error> => {
    try {
        const useUserStore = UserStore();

        let url = `applications/${application_id}/reviews`;
        url += `?user_id=${useUserStore.getUserId}`;
        if (step_id) {
            url += `&step_id=${step_id}`;
        }
        const response: any = await useMyFetch(url, {
            async beforeFetch({ url, options, cancel }) {
                return {
                    options
                };
            }
        }).get();
        const data: ApplicationReviewResponseModel[] = await handleResponse(
            response
        );
        return data;
    } catch (error) {
        handleError(error, 'Error finding reviews of the application !');
    }
};
