import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';
import {
    ApplicationResponseModel,
    JobResponseModel
} from 'talenthive-models/lib/Entities';

export const ApiFetchCompanyApplications = async (
    params
): Promise<ApplicationResponseModel[] | Error> => {
    const useUserStore = UserStore();
    try {
        const response: any = await useMyFetch('graphql', {
            async beforeFetch({ url, options, cancel }) {
                let queryArgs = '$company_id: [String!]!  $status: [String!]';
                const variables = {
                    company_id: useUserStore.getCompanies
                };

                //Build Args
                if (params.status) {
                    Object.assign(variables, { status: params.status });
                }

                const query = `query (${queryArgs}) {
                    jobs(company_id: $company_id) {
                        applications(status: $status) {
                            _id
                            job_id {
                                name
                                status
                            }
                            candidate_id {
                                firstname
                                lastname
                            }
                            status
                            createdAt
                        }
                    }
                }`;

                options.body = JSON.stringify({
                    query: query,
                    variables: variables
                });

                return {
                    options
                };
            }
        }).post();

        const data = await handleResponse(response);
        const jobs: JobResponseModel[] = data.jobs;
        const applications: ApplicationResponseModel[] = [];
        if (jobs.length > 0) {
            jobs.map((j) => applications.push(...j.applications));
        }

        return applications;
    } catch (e) {
        handleError(e, 'Cannot fetch Applications');
    }
};
