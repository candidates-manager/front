//Entities
import {
    ApplicationResponseModel,
    ApplicationUpdateModel
} from 'talenthive-models/lib/Entities';
import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateApplication = async (
    _id: string,
    params: ApplicationUpdateModel
): Promise<ApplicationResponseModel | Error> => {
    try {
        const response: any = await useMyFetch(`applications/u/${_id}`, {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).patch();
        const data: ApplicationResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
