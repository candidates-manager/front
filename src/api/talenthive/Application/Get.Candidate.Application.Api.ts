//Entities
import { ApplicationResponseModel } from 'talenthive-models/lib/Entities';

import { handleResponse, useMyFetchNoToken } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetCandidateApplication = async (
    _id: string,
    candidate_id: string
): Promise<ApplicationResponseModel | Error> => {
    try {
        const response: unknown = await useMyFetchNoToken(
            `applications/myApplication/${_id}?candidate_id=${candidate_id}`
        ).get();
        const data = <ApplicationResponseModel>await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, 'Error while trying to get application details !');
    }
};
