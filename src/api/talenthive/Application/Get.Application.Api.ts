//Entities
import { ApplicationResponseModel } from 'talenthive-models/lib/Entities';

import { handleResponse, useMyFetch } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetApplication = async (params: {
    _id: string;
}): Promise<ApplicationResponseModel | Error> => {
    try {
        const response: unknown = await useMyFetch(
            `applications/v/${params._id}`
        ).get();
        const data = <ApplicationResponseModel>await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, 'Error while trying to get application details !');
    }
};
