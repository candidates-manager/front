//Entities
import { RoleResponseModel } from 'talenthive-models/lib/Entities';

import { handleResponse, useMyFetch } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetRoles = async (params: {
    public_id: string[];
}): Promise<RoleResponseModel[] | Error> => {
    try {
        let url = 'roles';
        if (params.public_id.length > 0) {
            url += '?';
            for (let i = 0; i < params.public_id.length; i++) {
                url += `public_id[]=${params.public_id[i]}`;
                //Last element
                if (i != params.public_id.length - 1) url += '&';
            }
        }

        const response: unknown = await useMyFetch(url).get();

        const data = await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, 'Error while trying to get roles !');
    }
};
