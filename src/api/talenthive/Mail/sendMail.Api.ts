//Entities
import { MailEntity } from 'talenthive-models/lib/Entities';
import { handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiSendMail = async (
    mail: MailEntity,
    token?: string
): Promise<boolean | Error> => {
    try {
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        myHeaders.append('Authorization', `Bearer ${token}`);

        var requestOptions: any = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(mail)
        };

        const response = await fetch(
            `${import.meta.env.VITE_API_URL}/mail/send`,
            requestOptions
        );

        const data: boolean = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
