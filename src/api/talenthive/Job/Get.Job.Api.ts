//Entities
import { JobResponseModel } from 'talenthive-models/lib/Entities';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

//Api Config
import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Store
import { UserStore } from '@/store';

export const ApiGetCompanyJob = async (_id: string) => {
    try {
        const useUserStore = UserStore();
        const response: any = await useMyFetch('graphql', {
            async beforeFetch({ url, options, cancel }) {
                const query = `query($company_id: [String!]!, $_id: String!) {
                    jobs(company_id: $company_id, _id: $_id) {
                        name,
                        status
                        link
                        createdAt
                        applications {
                            _id
                            status
                            candidate_id {
                                firstname
                                lastname
                            }
                            createdAt
                            updatedAt
                        }
                    }
                }`;

                const variables = {
                    company_id: useUserStore.getCompanies,
                    _id: _id
                };

                options.body = JSON.stringify({
                    query: query,
                    variables: variables
                });

                return {
                    options
                };
            }
        }).post();

        const data = await handleResponse(response);
        const jobs: JobResponseModel[] = data.jobs;
        if (jobs[0]) return jobs[0];
        else throw new Error('Not Found');
    } catch (e) {
        handleError(e);
    }
};
