import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';
import { JobResponseModel } from 'talenthive-models/lib/Entities';

export const ApiFetchCompanyJobs = async (
    params
): Promise<JobResponseModel[] | Error> => {
    const useUserStore = UserStore();
    try {
        const response: any = await useMyFetch('graphql', {
            async beforeFetch({ url, options, cancel }) {
                let queryArgs = '$company_id: [String!]!';
                let jobsArgs = 'company_id: $company_id';
                const variables = {
                    company_id: useUserStore.getCompanies
                };

                //Build Args
                if (params.status) {
                    queryArgs += ' $status: [String!]';
                    jobsArgs += ' status: $status';
                    Object.assign(variables, { status: params.status });
                }

                const query = `query (${queryArgs}) {
                    jobs(${jobsArgs}) {
                        _id
                        
                        name
                        createdAt
                        status
                        application_process_id { 
                            name
                        }
                    }
                }`;

                options.body = JSON.stringify({
                    query: query,
                    variables: variables
                });

                return {
                    options
                };
            }
        }).post();

        const data = await handleResponse(response);
        const jobs: JobResponseModel[] = data.jobs;
        return jobs;
    } catch (e) {
        handleError(e);
    }
};
