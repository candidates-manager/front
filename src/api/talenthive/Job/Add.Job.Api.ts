//Entities
import {
    JobInputModel,
    JobResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiAddJob = async (
    params: JobInputModel
): Promise<JobResponseModel | Error> => {
    try {
        const response: any = await useMyFetch('jobs/add', {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).post();

        const data: JobResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
