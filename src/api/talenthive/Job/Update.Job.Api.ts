//Entities
import {
    JobResponseModel,
    JobUpdateModel
} from 'talenthive-models/lib/Entities';
import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateJob = async (
    _id: string,
    params: JobUpdateModel
): Promise<JobResponseModel | Error> => {
    try {
        const response: any = await useMyFetch(`jobs/u/${_id}`, {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).patch();

        const data: JobResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
