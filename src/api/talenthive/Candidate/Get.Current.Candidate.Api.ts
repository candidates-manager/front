//Entities
import { CandidateResponseModel } from 'talenthive-models/lib/Entities';

import { useMyFetchNoToken, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetCurrentCandidate = async (
    token: string
): Promise<CandidateResponseModel | Error> => {
    try {
        const response: unknown = await useMyFetchNoToken(
            `candidates/getCurrentCandidate/${token}`,
            {
                async beforeFetch({ url, options, cancel }) {
                    //Token
                    options.headers = {
                        'Content-Type': 'application/json'
                    };
                    return {
                        options
                    };
                }
            }
        ).get();
        const data = await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, "Error while trying to get candidate's details !");
    }
};
