//Entities
import {
    CandidateInputModel,
    CandidateResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiInviteCandidate = async (
    params: CandidateInputModel
): Promise<CandidateResponseModel | Error> => {
    try {
        const response = await useMyFetch('candidates/add', {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).post();

        const data: CandidateResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
