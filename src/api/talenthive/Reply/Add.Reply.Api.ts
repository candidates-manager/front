//Entities
import {
    ReplyInputModel,
    ReplyResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiAddReply = async (
    params: ReplyInputModel
): Promise<ReplyResponseModel | Error> => {
    try {
        const response = await useMyFetch('replies/add', {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).post();

        const data: ReplyResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
