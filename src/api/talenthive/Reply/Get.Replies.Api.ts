//Entities
import { ReplyResponseModel } from 'talenthive-models/lib/Entities';

import { handleResponse, useMyFetch } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetReplies = async (params: {
    entity_type: string;
    entity_id: string;
}): Promise<ReplyResponseModel[] | Error> => {
    try {
        const response: unknown = await useMyFetch(
            `replies/v/${params.entity_type}/${params.entity_id}`
        ).get();
        const data = <ReplyResponseModel[]>await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, 'Error while trying to get messages !');
    }
};
