import { useMyFetch, handleResponse } from '@ApiTalentHive/ApiConfig';
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';
import { UserResponseModel } from 'talenthive-models/lib/Entities';

export const ApiFetchCompanyUsers = async (
    params = {}
): Promise<UserResponseModel[] | Error> => {
    const useUserStore = UserStore();
    try {
        const response: any = await useMyFetch('graphql', {
            async beforeFetch({ url, options, cancel }) {
                let queryArgs = '$company_id: [String!]';
                let usersArgs = 'company_id: $company_id';
                const variables = {
                    company_id: useUserStore.getCompanies
                };

                const query = `query (${queryArgs}) {
                    users(${usersArgs}) {
                        _id
                        firstname
                        lastname
                        function
                    }
                }`;

                options.body = JSON.stringify({
                    query: query,
                    variables: variables
                });

                return {
                    options
                };
            }
        }).post();

        const data = await handleResponse(response);
        const users: UserResponseModel[] = data.users;
        return users;
    } catch (e) {
        handleError(e);
    }
};
