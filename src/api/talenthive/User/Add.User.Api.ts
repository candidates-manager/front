//Entities
import {
    UserInputModel,
    UserResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiAddUser = async (
    params: UserInputModel
): Promise<UserResponseModel | Error> => {
    try {
        const response: any = await useMyFetch('users/register', {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).post();

        const data: UserResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
