//Entities
import {
    UserResponseModel,
    UserUpdateModel
} from 'talenthive-models/lib/Entities';
import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateUser = async (
    _id: string,
    params: UserUpdateModel
): Promise<UserResponseModel | Error> => {
    try {
        const response: any = await useMyFetch(`users/u/${_id}`, {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).patch();
        const data: UserResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
