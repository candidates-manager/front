//Entities
import { UserResponseModel } from 'talenthive-models/lib/Entities';
import { handleResponse, useMyFetch } from '@/api/talenthive/ApiConfig';

//Store
import { UserStore } from '@/store';

export const ApiGetUsers = async (
    query: string
): Promise<UserResponseModel[] | Error> => {
    try {
        const userStore = UserStore();

        const response: unknown = await useMyFetch(`users?${query}`, {
            async beforeFetch({ url, options, cancel }) {
                return {
                    options
                };
            }
        }).get();
        const data = await handleResponse(response);
        return data;
    } catch (error) {}
};
