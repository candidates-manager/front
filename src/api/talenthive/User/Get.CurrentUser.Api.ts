//Entities
import { UserResponseModel } from 'talenthive-models/lib/Entities';
import { useMyFetchNoToken, handleResponse } from '@/api/talenthive/ApiConfig';

//Store
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetCurrentUser = async (
    token: string = null
): Promise<UserResponseModel | Error> => {
    try {
        const userStore = UserStore();

        if (!token) {
            token = userStore.getToken;
        }

        const response: unknown = await useMyFetchNoToken(
            `users/getCurrentUser`,
            {
                async beforeFetch({ url, options, cancel }) {
                    //Token
                    options.headers = {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`
                    };

                    return {
                        options
                    };
                }
            }
        ).get();
        const data = await handleResponse(response);
        return data;
    } catch (error) {
        // handleError(error, 'Error while trying to get user credentials !');
    }
};
