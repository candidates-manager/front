import { useMyFetch, handleResponse } from '@ApiTalentHive/ApiConfig';
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';
import {
    UserResponseModel,
    RoleResponseModel
} from 'talenthive-models/lib/Entities';

export const ApiFetchCompanyUsersByRoles = async (params: {
    roles: string[];
}): Promise<UserResponseModel[] | Error> => {
    const useUserStore = UserStore();
    try {
        const response: any = await useMyFetch('graphql', {
            async beforeFetch({ url, options, cancel }) {
                let queryArgs = '$public_id: [String!], $company_id: [String!]';

                let rolesArgs = 'public_id: $public_id';
                let usersArgs = 'company_id: $company_id';
                const variables = {
                    company_id: useUserStore.getCompanies,
                    public_id: params.roles
                };

                const query = `query (${queryArgs}) {
                    roles(${rolesArgs}) {
                        _id
                        public_id
                        name
                        users(${usersArgs}) {
                            _id
                            firstname
                            lastname
                            function
                            company_id {
                                name
                            }
                        }
                    }
                }`;

                options.body = JSON.stringify({
                    query: query,
                    variables: variables
                });

                return {
                    options
                };
            }
        }).post();

        const data = await handleResponse(response);
        const roles: RoleResponseModel[] = data.roles;
        const users: UserResponseModel[] = [];

        if (roles && roles.length > 0) {
            roles.map((r) => {
                const role = r.name;
                const roleUsers = r.users.map((u) => {
                    return { ...u, role: role };
                });

                users.push(...roleUsers);
            });
        }

        return users;
    } catch (e) {
        handleError(e);
    }
};
