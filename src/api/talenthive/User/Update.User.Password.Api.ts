//Entities
import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateUserPassword = async (
    _id: string,
    password: string
): Promise<Boolean | Error> => {
    try {
        const response: any = await useMyFetch(`users/u/password/${_id}`, {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify({ password });
                return {
                    options
                };
            }
        }).patch();
        const data: true = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
