import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';
import { ApplicationProcessResponseModel } from 'talenthive-models/lib/Entities';

export const ApiFetchCompanyProcesses = async (): Promise<
    ApplicationProcessResponseModel[] | Error
> => {
    const useUserStore = UserStore();
    try {
        const response: any = await useMyFetch('graphql', {
            async beforeFetch({ url, options, cancel }) {
                let queryArgs = '$company_id: [String!]';
                let processesArgs = 'company_id: $company_id';
                const variables = {
                    company_id: useUserStore.getCompanies
                };
                const query = `query (${queryArgs}) {
                    application_processses(${processesArgs}) {
                         _id
                        
                        company_id {
                            
                            name
                        }
                        name
                        updatedAt
                        createdAt
                        steps {
                            step
                            name
                            description
                            duration
                        }
                    }
                }`;

                options.body = JSON.stringify({
                    query: query,
                    variables: variables
                });

                return {
                    options
                };
            }
        }).post();

        const data = await handleResponse(response);
        const processes: ApplicationProcessResponseModel[] =
            data.application_processses;
        return processes;
    } catch (e) {
        handleError(e);
    }
};
