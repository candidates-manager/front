//Entities
import {
    ApplicationProcessUpdateModel,
    ApplicationProcessResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateApplicationProcess = async (
    _id: string,
    params: ApplicationProcessUpdateModel
): Promise<ApplicationProcessResponseModel | Error> => {
    try {
        const response: any = await useMyFetch(
            `applications/processes/u/${_id}`,
            {
                async beforeFetch({ url, options, cancel }) {
                    options.body = JSON.stringify(params);
                    return {
                        options
                    };
                }
            }
        ).patch();
        const data: ApplicationProcessResponseModel = await handleResponse(
            response
        );
        return data;
    } catch (error) {
        handleError(error, 'Error while updating your process !');
    }
};
