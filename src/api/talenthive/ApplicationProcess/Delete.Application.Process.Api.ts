import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiDeleteApplicationProcess = async (
    _id: string
): Promise<boolean | Error> => {
    try {
        const response: any = await useMyFetch(
            `applications/processes/d/${_id}`
        ).delete();
        const data: boolean = await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, 'Error while deleting your process !');
    }
};
