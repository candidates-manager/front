//Entities
import { ApplicationProcessResponseModel } from 'talenthive-models/lib/Entities';

import { handleResponse, useMyFetch } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetApplicationProcess = async (params: {
    _id: string;
}): Promise<ApplicationProcessResponseModel | Error> => {
    try {
        const response: unknown = await useMyFetch(
            `applications/processes/v/${params._id}`
        ).get();
        const data = <ApplicationProcessResponseModel>(
            await handleResponse(response)
        );
        return data;
    } catch (error) {
        handleError(
            error,
            'Error while trying to get application process details !'
        );
    }
};
