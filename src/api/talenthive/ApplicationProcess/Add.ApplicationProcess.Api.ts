//Entities
import {
    ApplicationProcessInputModel,
    ApplicationProcessResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiAddApplicationProcess = async (
    params: ApplicationProcessInputModel
): Promise<ApplicationProcessResponseModel | Error> => {
    try {
        const response: any = await useMyFetch('applications/processes/add', {
            async beforeFetch({ url, options, cancel }) {
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).post();
        const data: ApplicationProcessResponseModel = await handleResponse(
            response
        );
        return data;
    } catch (error) {
        handleError(error, 'Error while creating your process !');
    }
};
