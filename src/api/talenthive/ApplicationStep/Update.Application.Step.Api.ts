//Entities
import {
    ApplicationStepUpdateModel,
    ApplicationStepResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateApplicationStep = async (
    _id: string,
    params: ApplicationStepUpdateModel
): Promise<ApplicationStepResponseModel | Error> => {
    try {
        const response: any = await useMyFetch(`applications/steps/u/${_id}`, {
            async beforeFetch({ url, options, cancel }) {
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).patch();
        const data: ApplicationStepResponseModel = await handleResponse(
            response
        );
        return data;
    } catch (error) {
        handleError(error, 'Error while updating your step !');
    }
};
