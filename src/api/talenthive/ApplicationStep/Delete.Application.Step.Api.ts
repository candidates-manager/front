import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiDeleteApplicationStep = async (
    _id: string
): Promise<boolean | Error> => {
    try {
        const response: any = await useMyFetch(
            `applications/steps/d/${_id}`
        ).delete();
        const data: boolean = await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, 'Error while deleting your step !');
    }
};
