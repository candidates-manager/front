//Entities
import {
    ApplicationStepInputModel,
    ApplicationStepResponseModel
} from 'talenthive-models/lib/Entities';

import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiAddApplicationStep = async (
    params: ApplicationStepInputModel
): Promise<ApplicationStepResponseModel | Error> => {
    try {
        const response: any = await useMyFetch('applications/steps/add', {
            async beforeFetch({ url, options, cancel }) {
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).post();
        const data: ApplicationStepResponseModel = await handleResponse(
            response
        );
        return data;
    } catch (error) {
        handleError(error, 'Error while creating your step !');
    }
};
