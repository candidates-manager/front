//Entities
import {
    CompanyResponseModel,
    CompanyUpdateModel
} from 'talenthive-models/lib/Entities';
import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiUpdateCompany = async (
    _id: string,
    params: CompanyUpdateModel
): Promise<CompanyResponseModel | Error> => {
    try {
        const response: any = await useMyFetch(`company/u/${_id}`, {
            async beforeFetch({ url, options, cancel }) {
                //Token
                options.body = JSON.stringify(params);
                return {
                    options
                };
            }
        }).patch();
        const data: CompanyResponseModel = await handleResponse(response);
        return data;
    } catch (e) {
        handleError(e);
    }
};
