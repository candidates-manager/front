//Entities
import { CompanyResponseModel } from 'talenthive-models/lib/Entities';

import { handleResponse, useMyFetch } from '@/api/talenthive/ApiConfig';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';

export const ApiGetCompany = async (params: {
    _id: string;
}): Promise<CompanyResponseModel | Error> => {
    try {
        const response: unknown = await useMyFetch(
            `company/v/${params._id}`
        ).get();
        const data = <CompanyResponseModel>await handleResponse(response);
        return data;
    } catch (error) {
        handleError(error, 'Error while trying to get company details !');
    }
};
