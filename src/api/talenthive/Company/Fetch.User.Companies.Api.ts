import { useMyFetch, handleResponse } from '@/api/talenthive/ApiConfig';
import { UserStore } from '@/store';

//Handling Error
import { handleError } from '@Functions/handleEvents.js';
import { CompanyResponseModel } from 'talenthive-models/lib/Entities';

export const ApiFetchUserCompanies = async (): Promise<
    CompanyResponseModel[] | Error
> => {
    const useUserStore = UserStore();
    try {
        const response: any = await useMyFetch('graphql', {
            async beforeFetch({ url, options, cancel }) {
                const query = `query ($company_id: [String!]) {
                    companies(company_id: $company_id) {
                        _id
                        
                        name
                    }
                }`;

                const variables = {
                    company_id: useUserStore.getCompanies
                };

                options.body = JSON.stringify({
                    query: query,
                    variables: variables
                });

                return {
                    options
                };
            }
        }).post();

        const data = await handleResponse(response);
        const companies: CompanyResponseModel[] = data.companies;
        return companies;
    } catch (e) {
        handleError(e);
    }
};
